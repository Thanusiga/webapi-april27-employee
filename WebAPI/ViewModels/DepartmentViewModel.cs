﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.ViewModels
{
    public class DepartmentViewModel
    {

        [Required(ErrorMessage = "Please enter Department Name")]
        [Display(Name = "Department Name")]
        public String departmentName { get; set; }

        // Navigation Properties
        public ICollection<Employee> Employees { get; set; }

    }
}

