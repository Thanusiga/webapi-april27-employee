﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.ViewModels
{
    public class EmployeeViewModel : EditImageViewModel
    {
        [Required(ErrorMessage = "Please enter Employee Name")]
        public string EmployeeName { get; set; }


        // Navigation Properties
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }

    }
}
