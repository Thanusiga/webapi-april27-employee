﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeesController : Controller
    {
        private readonly EmployeeDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;

     
        public EmployeesController(EmployeeDbContext context, IWebHostEnvironment hostEnvironment)
        {
             _context = context;
            webHostEnvironment = hostEnvironment;
        }

        // GET: Employees
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var employeeDbContext = _context.Employees.Include(e => e.Department);
            return View(await employeeDbContext.ToListAsync());
        }

        // GET: Employees/Details/5
     
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Department)
                .FirstOrDefaultAsync(m => m.Id == id);

            var EmployeeViewModel = new EmployeeViewModel()
            {
                Id = employee.Id,
                EmployeeName = employee.EmployeeName,
                DepartmentId = employee.DepartmentId,
                ExistingImage = employee.EmployeeImage
            };

            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // GET: Employees/Create

        public IActionResult Create()
        {
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "departmentName");
            return View();
        }

        // POST: Employees/Create
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeName,EmployeeImage,DepartmentId")] EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {

                string uniqueFileName = ProcessUploadedFile(model);
                Employee employee = new Employee
                {
                    EmployeeName = model.EmployeeName,
                    DepartmentId = model.DepartmentId,
                    EmployeeImage = uniqueFileName
                };

                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "departmentName", model.DepartmentId);
            return View(model);
        }


        // GET: Employees/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id)        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);

            var EmployeeViewModel = new EmployeeViewModel()
            {
                Id = employee.Id,
                EmployeeName = employee.EmployeeName,
                DepartmentId = employee.DepartmentId,
                ExistingImage = employee.EmployeeImage
            };

            if (employee == null)
            {
                return NotFound();
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "departmentName", employee.DepartmentId);
            return View(EmployeeViewModel);
        }

        // POST: Employees/Edit/5
    //    [HttpPost, ActionName("Edit")]
      //  [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,EmployeeName,EmployeeImage,DepartmentId")] EmployeeViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                var employee = await _context.Employees.FindAsync(model.Id);
                employee.EmployeeName = model.EmployeeName;
                employee.DepartmentId = model.DepartmentId;

                if (model.EmployeeProfile != null)
                {
                    if (model.ExistingImage != null)
                    {
                        string filePath = Path.Combine(webHostEnvironment.WebRootPath, "Uploads", model.ExistingImage);
                        System.IO.File.Delete(filePath);
                    }

                    employee.EmployeeImage = ProcessUploadedFile(model);
                }
                _context.Update(employee);
                await _context.SaveChangesAsync();

                //try
                //{
                //    _context.Update(model);
                //    await _context.SaveChangesAsync();
                //}
                //catch (DbUpdateConcurrencyException)
                //{
                //    if (!EmployeeExists(model.Id))
                //    {
                //        return NotFound();
                //    }
                //    else
                //    {
                //        throw;
                //    }
                //}
                return RedirectToAction(nameof(Index));
            }

      
        ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "departmentName", model.DepartmentId);
            return View();
        }

        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Department)
                .FirstOrDefaultAsync(m => m.Id == id);

            var EmployeeViewModel = new EmployeeViewModel()
            {
                Id = employee.Id,
                EmployeeName = employee.EmployeeName,
                DepartmentId = employee.DepartmentId,
                ExistingImage = employee.EmployeeImage
            };



            if (employee == null)
            {
                return NotFound();
            }

            return View(EmployeeViewModel);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            var CurrentImage = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", employee.EmployeeImage);
            _context.Employees.Remove(employee);
            
            if(await _context.SaveChangesAsync() > 0)
            {
                if (System.IO.File.Exists(CurrentImage))
                {
                    System.IO.File.Delete(CurrentImage);
                }
            }

            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }

        private string ProcessUploadedFile(EmployeeViewModel model)
        {
            string uniqueFileName = null;

            if (model.EmployeeProfile != null)
            {
                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "Uploads");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.EmployeeProfile.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.EmployeeProfile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }
    }
}
